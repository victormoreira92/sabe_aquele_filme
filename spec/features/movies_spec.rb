require 'rails_helper'

RSpec.feature "Movies", type: :feature do
  describe "#index" do
    context "section#banner" do
      it "button#Veja mais" do
        visit(welcome_index_path)
        click_link 'Veja Mais'
        expect(page).to  have_current_path(movies_path)
      end
    end

    
  end
  
  describe "#movies_path" do
    before (:example) do
      200.times{create(:movie)}
      visit(movies_path)
    end

    context "section#movies" do
      it "mostrar pagina de um filme clicado" do
       find('a.link_movie_12').click
       expect(current_path).to eq(movie_path(12)) 
      end

    end
  end
  
  describe "#show" do
    before (:example) do
      @movie = create(:movie, id: 1, title: "Life of Pi", release_year: 1980, runtime: 125)
      visit(movie_path(@movie.id))
    end

    context "pagina de filme" do
      it "com nome e ano de filme" do
        expect(current_path).to eq(movie_path(@movie.id)) 
        expect(page).to have_content(@movie.title)
        expect(page).to have_content(@movie.release_year)
      end

      it "button#back to movies" do
        click_link('Back to movies')
        expect(current_path).to eq(movies_path) 
      end

      it "button#veja a avaliação" do
        click_link('Veja a avaliacação para '+@movie.title)
        expect(current_path).to eq(rate_path(@movie.rate.id)) 
      end
      
    end
  end
  
end
