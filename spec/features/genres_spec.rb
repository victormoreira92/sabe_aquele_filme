require 'rails_helper'

RSpec.feature "Genres", type: :feature do
  before (:example) do
    @drama_genre = create(:movie, genre: (create(:genre, :Drama)))
    create(:movie, genre: (create(:genre, :Terror)))
    create(:movie, genre: (create(:genre, :Adventure)))
    create(:movie, genre: (create(:genre, :Action)))
    visit(genres_path)  
  end
  
  describe "page" do
    context "#index" do
      it "mostrar nome de genero" do
        expect(page).to have_content("Drama")
      end

      it "link para mostrar genero" do
        click_link('Veja mais filmes de '+@drama_genre.genre.title_genre)
        expect(current_path).to eq(genre_path(@drama_genre.genre.id)) 
      end
      
    end
  end
  
end
