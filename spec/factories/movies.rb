FactoryBot.define do
  factory :movie do
    title {Faker::Movie.title}
    resume {LoremIpsum.lorem_ipsum(paragraphs: 2)}
    runtime {rand(70..300)}
    release_year {rand(1925..2023)}
    url_poster {"https://dummyimage.com/170x271/8aff05/000000.png&text=fake"}
    url_large_image {"https://dummyimage.com/170x271/8aff05/000000.png&text=fake"}
    association :genre
    association :rate
  end

  trait :movie_with_genre_rate do
    after(:create) do |movie|
      create(:genre, movie: movie.id)
      create(:rate, movie: movie.id)
    end
  end

  

end
