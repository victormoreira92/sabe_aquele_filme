FactoryBot.define do
  factory :rate do
    score {rand(10..100)}
    commentary {LoremIpsum.lorem_ipsum(140)}
    time_rate {Time.local(2022, 1, 1)}
    movies {[]}
  end

  trait :good do
    score {rand(61..80)}
  end

  trait :bad do
    score {rand(0..30)}
  end

  trait :regular do
    score {rand(31..60)}
  end

  trait :must_see do
    score {rand(81..100)}
  end

end
