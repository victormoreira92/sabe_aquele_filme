FactoryBot.define do
  factory :genre do
    movies {[]}
    title_genre {Faker::Name.name}
    description_genre {LoremIpsum.lorem_ipsum(275)}
  end

  trait :western do
    title_genre { "western" }
  end

  trait :comedy do
    title_genre { "comedy" }
  end

  trait :scifi do
    title_genre { "science-fiction" }
  end

  trait :Adventure do
    title_genre { "Adventure" }
  end

  trait :Drama do
    title_genre { "Drama" }
  end

  trait :Action do
    title_genre { "Action" }
  end

  trait :Terror do
    title_genre { "Terror" }
  end

end
