require 'rails_helper'

RSpec.describe Genre, type: :model do
  describe "#atributos" do
    let(:genre) { create(:genre)}

    context 'title_genre' do

      it "com valor aleatorio" do
        expect(genre).to be_valid
      end

      it "com valor nil" do
       genre.title_genre = nil
       expect(genre).to be_invalid
      end

      it "com descrition_genre vazio" do
       expect(genre).to be_valid
      end

      it "acima do titulo aceito" do
        genre.title_genre = LoremIpsum.lorem_ipsum(52)
        expect(genre).to be_invalid
      end

      it "abaixo do titulo aceito" do
        genre.title_genre = LoremIpsum.lorem_ipsum(1)
        expect(genre).to be_invalid
      end

    end
    
    context "description_genre" do
      it "acima de 280 caracteres" do
        genre.description_genre = LoremIpsum.lorem_ipsum(285)
        expect(genre).to be_invalid
      end

      it "abaixo de 140 caracteres" do
        genre.description_genre = LoremIpsum.lorem_ipsum(10)
        expect(genre).to be_invalid
      end
      
    end
    
  end

  describe "#gêneros aceitos" do
    let(:genre_western) {create(:genre, :western)} 
    context "under condition" do
      it "behaves like" do
        expect(genre_western.title_genre).to eq("western") 
      end
    end
  end
  
end
