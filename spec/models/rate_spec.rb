require 'rails_helper'

RSpec.describe Rate, type: :model do
  describe "#atributos" do
    let(:rate) { create(:rate)}
    specify{expect(rate).to be_valid}

    context "score" do
      it "com valor vazio" do
        rate.score = nil
        expect(rate).to be_invalid
      end

      it"com valor acima de 100" do
       rate.score = 112
       expect(rate).to be_invalid
      end

      it"com valor abaixo de 0" do
        rate.score = 0
        expect(rate).to be_invalid
      end
      
    end

    context "comemtary" do
      
      it "com caracteres abaixo do permitido" do
        rate.commentary = LoremIpsum.lorem_ipsum(10)
        expect(rate).to be_invalid
      end

      it "com caracteres acima do permitido" do
        rate.commentary = LoremIpsum.lorem_ipsum(298)
        expect(rate).to be_invalid
      end
    end

    describe "#funções" do
      let(:rate) { create(:rate)}
      context "classify_rate" do
        it "para Rate bad" do
          rate.score = 15
          expect(rate.classify_rate(rate.score)).to eq("bad")
        end

        it "para Rate good" do
          rate.score = 47
          expect(rate.classify_rate(rate.score)).to eq("regular")
        end

        it "para Rate regular" do
          rate.score = 75
          expect(rate.classify_rate(rate.score)).to eq("good")
        end

        it "para Rate must-see" do
          rate.score = 85
          expect(rate.classify_rate(rate.score)).to eq("must-see")
        end
      end
    end
    
    
  end

end
