require 'rails_helper'

RSpec.describe Movie, type: :model do
  describe "#atributos" do
    context "Testes atributos com filme definido" do 
      let(:guerra_nas_estrelas) {create(:movie,
        title: "Guerra nas Estrelas: O Império Contra-Ataca",
        runtime: 124,
        release_year: 1980)} 
  
      it "Filme deve possuir o nome Guerra nas Estrelas: O Império Contra-Ataca" do
        expect(guerra_nas_estrelas.title).to  eq("Guerra nas Estrelas: O Império Contra-Ataca")
      end
  
      it "Filme deve possuir ano de lançamento em 1980" do
        expect(guerra_nas_estrelas.release_year).to  eq(1980)
      end
      
      it "Filme deve possuir duração em 124 minutos" do
        expect(guerra_nas_estrelas.runtime).to  eq(124)
      end
  
    end
    
    context "Testes de atributos com filme aleatorio" do
      let(:filme_aleatorio){create(:movie)}
  
      it "Filme deve possuir ano de lançamento dentro entre 1927 a 2023" do
        expect(filme_aleatorio.release_year).to be_between(1927,2023)
      end
  
      it "Filme deve duração entre 70 a 300 minutos" do
        expect(filme_aleatorio.runtime).to be_between(70,300)
      end
    end
  end

  describe "#funções de modelo" do
    let(:movie){create(:movie)}

    context "Teste com funções do Modelo filme" do
      it "Função runtime_hour deve retornar em 2h 55min para 175 minutos" do
        movie.runtime = 175
        expect(movie.runtime_hour(movie.runtime)).to eq("2h 55min")
      end

    end
  end
  
end
