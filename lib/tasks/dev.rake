namespace :dev do
  desc "Configura o ambiente de desenvolvimento"
  task setup: :environment do
    if Rails.env.development?
      show_spinner("Apagando BD"){puts %x(rails db:drop)}
      show_spinner("Criando BD"){puts %x(rails db:create )}
      show_spinner("Migrando scaffold para o BD"){puts %x(rails db:migrate)}
      %x(rails dev:add_genres)
      %x(rails dev:add_rates)
      %x(rails dev:add_movies)
    end
  end

  desc "Adicionar filmes ao banco de dados"
  task add_movies: :environment do
    show_spinner("Casdastrando os filmes...") do
      movies = []
      200.times{
      |i| 
      name = Faker::Movie.title
      tmp = {
        title: "#{name}",
        release_year: rand(1925..2023),
        resume:" Indiana Jones as he sets out to find his 
        missing father, Dr. Henry Jones Sr. (played by Sean Connery),
        who was also searching for the Holy Grail, a legendary 
        artifact said to grant eternal life. Jones receives a 
        diary belonging to his father, which contains clues to 
        the Grail's location. He travels to Venice to search 
        for his father and meets Elsa Schneider (played by Alison 
        Doody), a beautiful Austrian art historian who offers 
        to help him.",
        runtime: rand(70..300),
        url_poster: "https://dummyimage.com/170x271/8aff05/000000.png&text=#{name}",
        url_large_image:"https://dummyimage.com/600x400/8aff05/000000.png&text=#{name}",
        genre: Genre.all.sample,
        rate: Rate.all.sample
      }
      movies << tmp
      }
      movies.each do |movie|
        Movie.find_or_create_by!(movie)
      end
    end
  end

  desc "Adicionar generos ao banco de dados"
  task add_genres: :environment do
    show_spinner("Casdastrando os generos...") do
      genres = [
        {
          title_genre:"Science-Fiction",
          description_genre: "Filmes que abordam eleme
          ntos como viagens no tempo, exploração espacial, 
          robótica, inteligência artificial, futuros distópicos 
          ou utópicos, e outras ideias que desafiam a compreensão 
          convencional do mundo"
        },
        {
          title_genre:"Comedy",
          description_genre: "Filmes que apresentam situações cômicas,
           personagens engraçados e diálogos bem-humorados,
           baseado em diversas formas, como sátira, ironia, 
           humor físico, humor negro, entre outros"
        },
        {
          title_genre:"Drama",
          description_genre: "situações de conflito e tensão 
          emocional, geralmente envolvendo personagens em situações 
          difíceis e dramáticas."
        },
        {
          title_genre:"Adventure",
          description_genre: "gênero que se caracteriza por 
          apresentar histórias emocionantes 
          e empolgantes de exploração, descoberta e perigo"
        },
        {
          title_genre:"Action",
          description_genre: " Filmes que se concentram em cenas de movimento 
          e energia, geralmente envolvendo perseguições, lutas, explosões 
          e tiroteios."
        },
        {
          title_genre:"Terror",
          description_genre: "Filmes que provocam medo e horror no público, através de uma 
          narrativa que envolve elementos sobrenaturais, ameaças e situações assustadoras"
        },
        {
          title_genre:"Suspense",
          description_genre: "Suspense é um gênero artístico que mantém 
          o público em estado de tensão e expectativa, através de uma 
          narrativa intrigante e envolvente, geralmente envolvendo 
          perigo iminente e situações misteriosas."
        },
        {
          title_genre:"Romance",
          description_genre: "Filmes que se concentram no desenvolvimento 
          de relações amorosas entre personagens, abrangendo uma ampla 
          variedade de temas, como amor, amizade, família, perda, 
          superação, entre outros."
        },
        {
          title_genre:"Western",
          description_genre: "Filmes retratam a cultura e o ambiente do Velho Oeste 
          americano, com enredos frequentemente centrados em cowboys, 
          pistoleiros, xerifes e outros personagens típicos dessa época, 
          envolvendo aventura, conflitos e ação."
        }
      ]
      genres.each do |genre|
        Genre.find_or_create_by!(genre)
      end
    end
  end
  
  desc "Adicionar avaliacao ao banco de dados"
  task add_rates: :environment do
    show_spinner("Casdastrando os avaliacao...") do
      rates = []
      150.times{
        |i|
        tmp = {
          score: rand(1..100),
          commentary: LoremIpsum.lorem_ipsum(paragraphs: 2),
          time_rate: time_rand(Time.local(2022, 1, 1))
        }
        rates << tmp
      }
      rates.each do |rate|
        Rate.find_or_create_by!(rate)
      end
    end
  end
  
  private 
  def show_spinner(msg_start, msg_end = "Concluido")
    spinner = TTY::Spinner.new("[:spinner] #{msg_start} ...")
    spinner.auto_spin
    yield
    spinner.success("(#{msg_end})")
  end
  def time_rand(from = 0.0, to = Time.now)
    Time.at(from + rand * (to.to_f - from.to_f))
  end
end
