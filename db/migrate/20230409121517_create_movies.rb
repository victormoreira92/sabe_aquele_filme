class CreateMovies < ActiveRecord::Migration[7.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.text :resume
      t.integer :release_year
      t.integer :runtime
      t.string :url_poster
      t.string :url_large_image

      t.timestamps
    end
  end
end
