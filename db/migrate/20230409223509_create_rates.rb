class CreateRates < ActiveRecord::Migration[7.0]
  def change
    create_table :rates do |t|
      t.integer :score
      t.text :commentary
      t.datetime :time_rate

      t.timestamps
    end
  end
end
