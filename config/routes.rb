Rails.application.routes.draw do
  get 'filter_movie/index'
  resources :rates
  resources :genres
  resources :movies
  get 'welcome/index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "welcome#index"
end
