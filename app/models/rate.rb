class Rate < ApplicationRecord
    has_many :movies
    validates :score, presence: true,  numericality: {in: (1..100)}
    validates :commentary, length: (140..280)

    def classify_rate(rate)
        classification = {0..30 => "bad",31..60 => "regular", 61..80 => "good", 81..100 => "must-see"}
        classification.select{|range| range === rate}.values.first
    end
        
end
