class Genre < ApplicationRecord
    has_many :movies
    validates :title_genre, presence: true, length: (2..30)
    validates :description_genre, length: (140..280)

end
