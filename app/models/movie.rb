class Movie < ApplicationRecord
    belongs_to :genre 
    belongs_to :rate

    def runtime_hour(hour)
        "#{hour/60}h #{hour % 60}min"
    end
end
