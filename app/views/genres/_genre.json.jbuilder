json.extract! genre, :id, :title_genre, :description_genre, :created_at, :updated_at
json.url genre_url(genre, format: :json)
