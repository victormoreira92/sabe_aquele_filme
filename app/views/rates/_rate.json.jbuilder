json.extract! rate, :id, :score, :commentary, :time_rate, :created_at, :updated_at
json.url rate_url(rate, format: :json)
