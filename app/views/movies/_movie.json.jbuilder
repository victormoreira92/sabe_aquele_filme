json.extract! movie, :id, :title, :resume, :release_year, :runtime, :url_poster, :url_large_image, :created_at, :updated_at
json.url movie_url(movie, format: :json)
