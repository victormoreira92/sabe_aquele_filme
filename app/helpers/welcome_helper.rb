module WelcomeHelper
    def truncate_text (text)
        text.first(150)+"..."
    end
    def class_rate(rate)
        rates_rate = {0..30 => "bg-danger",31..60 => "bg-warning", 60..100 => "bg-success"}
        rates_rate.select{|range| range === rate}.values.first
    end
end
